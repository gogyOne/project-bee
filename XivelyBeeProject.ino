/*
  Xively sensor client with Strings

 This sketch connects an analog sensor to Xively,
 using an Arduino Yún.

 created 15 March 2010
 updated 27 May 2013
 by Tom Igoe

 http://arduino.cc/en/Tutorial/YunXivelyClient

 */


// include all Libraries needed:
#include <Process.h>
#include "passwords.h"      // contains my passwords, see below
#include <DistanceGP2Y0A21YK.h>


/*
  NOTE: passwords.h is not included with this repo because it contains my passwords.
 You need to create it for your own version of this application.  To do so, make
 a new tab in Arduino, call it passwords.h, and include the following variables and constants:

 #define APIKEY        "foo"                  // replace your pachube api key here
 #define FEEDID        0000                   // replace your feed ID
 #define USERAGENT     "my-project"           // user agent is the project name
 */


// set up net client info:
const unsigned long postingInterval = 5250;  //delay between updates to xively.com
unsigned long lastRequest = 0;      // when you last made a request
String dataString = "";
//pin del sensor PIR
int pinSensor = 6;
//variable que guarda el conteo
long suma = 0;
//guarda el cambio de movimiento
String out = "";
//---
unsigned long previousMillis = 0;
//---
boolean resp = false;
//----
//define pin de sensor de humedad de suelo
int pinHumedad=A2;
//objeto distancia
DistanceGP2Y0A21YK Dist;
//pin sensor ir
int pinIR=A3;

void setup() {
  // start serial port:
  Bridge.begin();
  Serial.begin(9600);
  while (!Serial);   // wait for Network Serial to open
  Serial.println("Xively client");
  pinMode(pinSensor, INPUT);
  //calibracion

  Serial.println("Calibrando");
  for (int cont = 0; cont < 60; cont++) {
    Serial.print(".");
    delay(1000);
  }
  Serial.println("");
  Serial.println("Sensor Activado");
  //configuracion de sensor de humedad
  pinMode(pinHumedad, INPUT);
  Dist.begin(pinIR);
}

void loop() {
  // get a timestamp so you can calculate reading and sending intervals:
  long now = millis();

  // if the sending interval has passed since your
  // last connection, then connect again and send data:
  if (now - lastRequest >= postingInterval) {
    updateData();
    if (resp == true) {
      sendData();
    }
    lastRequest = now;
  }
}

void updateData() {
  Serial.println("Actualizando datos...");
  // convert the readings to a String to send it:
  dataString = "Conteo,";
  dataString += getConteo();
  // add pressure:
  dataString += "\nHumedad,";
  dataString += String(getHumedad());
  dataString += "\nDistancia,";
  dataString += String(getDistancia());
}
//obtiene la distancia en centimetros
int getDistancia(){
  return Dist.getDistanceCentimeter();
}
//obtiene un conteo
String getConteo() {
  resp = false;
  String resultado = "";
  //se repite este codigo cada (1 segundo)/4
  //unsigned long currentMillis = millis();
  //if (currentMillis - previousMillis >= 250) {
  //previousMillis = currentMillis;
  if (digitalRead(pinSensor) == HIGH) {
    //si hay movimiento
    Serial.println("movimiento");
    out = "0";
  }
  else {
    //en caso de "no movimiento"
    Serial.println("no");
    out += "1";
  }
  /*Si el sensor marca movimiento y despues no hay movimiento,
  se realiza el conteo
  */
  if (out.length() >= 2 && (out.charAt(0) == '0' && out.charAt(1) == '1')) {
    //se resetea la variable
    out = "";
    //se realiza el conteo
    suma++;
    resultado += suma;
    resp = true;
  }
  //muestra el conteo
  Serial.print("suma es ");
  Serial.println(suma);
  //}
  return resultado;
}
// this method makes a HTTP connection to the server:
void sendData() {
  Serial.println("Enviando datos a xively...");
  // form the string for the API header parameter:
  String apiString = "X-ApiKey: ";
  apiString += APIKEY;

  // form the string for the URL parameter:
  String url = "https://api.xively.com/v2/feeds/";
  url += FEEDID;
  url += ".csv";

  // Send the HTTP PUT request

  // Is better to declare the Process here, so when the
  // sendData function finishes the resources are immediately
  // released. Declaring it global works too, BTW.
  Process xively;
  Serial.print("\n\nSending data... ");
  xively.begin("curl");
  xively.addParameter("-k");
  xively.addParameter("--request");
  xively.addParameter("PUT");
  xively.addParameter("--data");
  xively.addParameter(dataString);
  xively.addParameter("--header");
  xively.addParameter(apiString);
  xively.addParameter(url);
  xively.run();
  Serial.println("done!");

  // If there's incoming data from the net connection,
  // send it out the Serial:
  while (xively.available() > 0) {
    char c = xively.read();
    Serial.write(c);
  }

}
//obtener humedad en porcentaje
double getHumedad(){
  //lee el pin de humedad
  int readHum=analogRead(pinHumedad);
  //convierte a porcentaje
  double porcentaje=(double)100*readHum/1024.00;
  Serial.print("Humedad en crudo: ");
  Serial.println(readHum);
  Serial.print("porcentaje de humedad: ");
  Serial.println(porcentaje,2);
  if(readHum<=300){
    Serial.println("Tierra seca");
  }else if(readHum<=600){
    Serial.println("Tierra humeda");
  }else {
    Serial.println("Tierra mojada");
  }
  return porcentaje;
}
